<?php

namespace Rocket\Translations;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Translation extends Eloquent
{
    protected $table = "rocket_translations";
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'translation', 'language_code', 'translated',
    ];

    public static function getTranslations($language, $group, $namespace = null)
    {
        return static::queryTranslations($language, $group, $namespace);
    }

    protected static function queryTranslations($language, $group, $namespace = null)
    {
        $key = '';

        if (!is_null($namespace) && $namespace !== '*') {
            $key .= "{$namespace}::";
        }

        $key .= "{$group}.%";

        $result = static::query()
            ->select('translation', 'translated')
            ->where('translation', 'like', $key)
            ->where('language_code', $language)
            ->get();

        $translations = $result->reduce(function($translations, Translation $translation) use ($group) {
            if (!is_array($translations)) {
                $translations = [];
            }

            $position = strpos($translation->translation, "{$group}.") + strlen("{$group}.");

            $key = substr($translation->translation, $position);
            // save as flat array
            $translations[$key] = $translation->translated;

            // save as recursive array
            $keys = explode('.', $key);

            $trans = $translation->translated;
            foreach (array_reverse($keys) as $key) {
                $trans = array($key => $trans);
            }

            $translations = array_replace_recursive($translations, $trans);

            return $translations;
        });

        return $translations ?? [];
    }

    public static function getCacheKey($language, $group, $namespace)
    {
        return "rocket.translations.{$language}.{$group}.{$namespace}";
    }


    protected function flushCache()
    {
    }
}