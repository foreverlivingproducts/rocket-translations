<?php

namespace Rocket\Translations\Providers;

use Illuminate\Translation\TranslationServiceProvider as IlluminateTranslationServiceProvider;
use Rocket\Translations\TranslationManager;

class TranslationServiceProvider extends IlluminateTranslationServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations/');
    }

    /**
     * Define the resources used by Rocket.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../../config/translations.php', 'translations');
        parent::register();
    }

    protected function registerLoader()
    {
        $this->app->singleton('translation.loader', function ($app) {
            return new TranslationManager($app['files'], $app['path.lang']);
        });
    }
}