<?php

namespace Rocket\Translations;

use Illuminate\Translation\FileLoader;

class TranslationManager extends FileLoader
{
    /**
     * Load the messages for the given locale.
     *
     * @param string $language
     * @param string $group
     * @param string $namespace
     *
     * @return array
     */
    public function load($language, $group, $namespace = null): array
    {
        $fileTranslations   = parent::load($language, $group, $namespace);
        $translations       = $this->getTranslations($language, $group, $namespace);

        if (config('translations.insert_new_translations')) {
            $this->insertTranslations($language, $group, $namespace, $fileTranslations, $translations);
        }

        return array_replace_recursive($fileTranslations, $translations);
    }

    protected function getTranslations($language, $group, $namespace = null)
    {
        $loaders = collect(config('translations.translation_loaders'));

        $translations = $loaders->mapWithKeys(function ($className) use ($language, $group, $namespace) {
            $loader = app($className);

            return $loader->loadTranslations($language, $group, $namespace);
        });

        return $translations->toArray();
    }

    protected function insertTranslations($language, $group, $namespace, $fileTranslations, $translations)
    {
        $translationKey = '';

        if (!is_null($namespace) && $namespace !== '*') {
            $translationKey .= "{$namespace}::";
        }

        $fileTranslationsWithGroup = array_dot([$group => $fileTranslations]);
        $translationsWithGroup = array_dot([$group => $translations]);

        $fileCollection = collect($fileTranslationsWithGroup);
        $translationCollection = collect($translationsWithGroup);

        $translationsDifference = $fileCollection->diffKeys($translationCollection);

        $translationsDifference
            ->each(function($key,$value) use ($language, $namespace, $translationKey) {
                if (isset($key) && isset($value)
                    && is_string($key) && is_string($value)){
                    try {
                        Translation::insert([
                            'language_code' => $language,
                            'translated' => $key,
                            'translation' => $translationKey . $value
                        ]);
                    } catch (\Exception $e) {
                        // Silence is bliss
                    }
                }
            });

    }
}