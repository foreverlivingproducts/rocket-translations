<?php

return [
    'translation_loaders' => [
        Rocket\Translations\TranslationManager\Loader::class,
    ],

    'model' => Rocket\Translations\Translation::class,

    /*
     * insert_new_translations will activate functionality to insert all
     * file translations into database if translations is none existing in database.
     *
     * Default: 'insert_new_translations' => true
    */
    'insert_new_translations' => true,
];